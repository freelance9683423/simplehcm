# SimpleHCM

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.x.x.

## Originality of code

This project has been built on a template from Keenthemes. All menus and website application pages in this project have been rebuilt according to the specifications provided by the client.

## Run on your local

To run this project on a local server, you must download nodejs, npm and angular globally first. When "npm install" an error will appear, you don't need to worry. This error was caused by a conflict between one of the plugins in this template. So, to run it you have to complete the script again with "npm install --force", wait a few moments to be able to run this project on your local server.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Support

If you have further problems, you can contact me via email: rizki.fajar.work@gmail.com
