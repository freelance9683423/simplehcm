import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EditempComponent } from './editemp.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: EditempComponent,
      },
    ]),
  ],
})
export class EditempModule { }
