import { Component } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputNumberModule } from 'primeng/inputnumber';
import { formatCurrency } from '@angular/common';

@Component({
  selector: 'app-addemp',
  templateUrl: './editemp.component.html',
  styleUrl: './editemp.component.scss',
  standalone: true,
  imports: [InputNumberModule,DropdownModule,MultiSelectModule,MatCardModule,MatFormFieldModule, MatInputModule, FormsModule, ReactiveFormsModule],
})
export class EditempComponent 
{
  email: string = "test1@gmail.com";
  username: string = "test1";
  firstname: string = "satria";
  lastname: string = "sanusi"
  birthdate: string = "1990-10-10";
  basicsalary = formatCurrency(10000000, 'en-IDR', 'Rp.', 'IDR', '1.0-0');
  status: string = "PKWT";
  group: string = "MARKETING"; 
  joindate: string = "2023-10-10";
  signupForm: FormGroup;
  constructor() {}  
}
