import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeData {
    getData() {
        return [
            {
                id: 1000,
                username: 'Tes1',
                firstname: 'Tes1',
                lastname: 'Tes1',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1001,
                username: 'Tes2',
                firstname: 'Tes2',
                lastname: 'Tes2',
                email: 'Tes2@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1002,
                username: 'Tes3',
                firstname: 'Tes3',
                lastname: 'Tes3',
                email: 'Tes3@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1003,
                username: 'Tes4',
                firstname: 'Tes4',
                lastname: 'Tes4',
                email: 'Tes4@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1004,
                username: 'Tes5',
                firstname: 'Tes5',
                lastname: 'Tes5',
                email: 'Tes5@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1005,
                username: 'Tes6',
                firstname: 'Tes6',
                lastname: 'Tes6',
                email: 'Tes6@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1006,
                username: 'Tes7',
                firstname: 'Tes7',
                lastname: 'Tes7',
                email: 'Tes7@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1007,
                username: 'Tes8',
                firstname: 'Tes8',
                lastname: 'Tes8',
                email: 'Tes8@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1008,
                username: 'Tes9',
                firstname: 'Tes9',
                lastname: 'Tes9',
                email: 'Tes9@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1009,
                username: 'Tes10',
                firstname: 'Tes10',
                lastname: 'Tes10',
                email: 'Tes10@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1010,
                username: 'Tes11',
                firstname: 'Tes11',
                lastname: 'Tes11',
                email: 'Tes11@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1011,
                username: 'Tes12',
                firstname: 'Tes12',
                lastname: 'Tes12',
                email: 'Tes12@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1012,
                username: 'Tes13',
                firstname: 'Tes13',
                lastname: 'Tes13',
                email: 'Tes13@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1013,
                username: 'Tes14',
                firstname: 'Tes14',
                lastname: 'Tes14',
                email: 'Tes14@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes15',
                firstname: 'Tes15',
                lastname: 'Tes15',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes16',
                firstname: 'Tes16',
                lastname: 'Tes16',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes17',
                firstname: 'Tes17',
                lastname: 'Tes17',
                email: 'Tes17@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes18',
                firstname: 'Tes18',
                lastname: 'Tes18',
                email: 'Tes18@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes19',
                firstname: 'Tes19',
                lastname: 'Tes19',
                email: 'Tes19@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes20',
                firstname: 'Tes20',
                lastname: 'Tes20',
                email: 'Tes20@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes21',
                firstname: 'Tes21',
                lastname: 'Tes21',
                email: 'Tes21@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes22',
                firstname: 'Tes22',
                lastname: 'Tes22',
                email: 'Tes22@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes23',
                firstname: 'Tes23',
                lastname: 'Tes23',
                email: 'Tes23@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes24',
                firstname: 'Tes24',
                lastname: 'Tes24',
                email: 'Tes24@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes25',
                firstname: 'Tes25',
                lastname: 'Tes25',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes26',
                firstname: 'Tes26',
                lastname: 'Tes26',
                email: 'Tes26@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes27',
                firstname: 'Tes27',
                lastname: 'Tes27',
                email: 'Tes27@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes28',
                firstname: 'Tes28',
                lastname: 'Tes28',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes29',
                firstname: 'Tes29',
                lastname: 'Tes29',
                email: 'Tes29@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes30',
                firstname: 'Tes30',
                lastname: 'Tes30',
                email: 'Tes30@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes31',
                firstname: 'Tes31',
                lastname: 'Tes31',
                email: 'Tes31@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes32',
                firstname: 'Tes32',
                lastname: 'Tes32',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes33',
                firstname: 'Tes33',
                lastname: 'Tes33',
                email: 'Tes33@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes34',
                firstname: 'Tes34',
                lastname: 'Tes34',
                email: 'Tes34@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes35',
                firstname: 'Tes35',
                lastname: 'Tes35',
                email: 'Tes35@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes36',
                firstname: 'Tes36',
                lastname: 'Tes36',
                email: 'Tes36@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes1',
                firstname: 'Tes1',
                lastname: 'Tes1',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes2',
                firstname: 'Tes2',
                lastname: 'Tes2',
                email: 'Tes2@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes3',
                firstname: 'Tes3',
                lastname: 'Tes3',
                email: 'Tes3@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes4',
                firstname: 'Tes4',
                lastname: 'Tes4',
                email: 'Tes4@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes5',
                firstname: 'Tes5',
                lastname: 'Tes5',
                email: 'Tes5@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes6',
                firstname: 'Tes6',
                lastname: 'Tes6',
                email: 'Tes6@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes7',
                firstname: 'Tes7',
                lastname: 'Tes7',
                email: 'Tes7@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes8',
                firstname: 'Tes8',
                lastname: 'Tes8',
                email: 'Tes8@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes9',
                firstname: 'Tes9',
                lastname: 'Tes9',
                email: 'Tes9@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes10',
                firstname: 'Tes10',
                lastname: 'Tes10',
                email: 'Tes10@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes11',
                firstname: 'Tes11',
                lastname: 'Tes11',
                email: 'Tes11@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes12',
                firstname: 'Tes12',
                lastname: 'Tes12',
                email: 'Tes12@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes13',
                firstname: 'Tes13',
                lastname: 'Tes13',
                email: 'Tes13@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes14',
                firstname: 'Tes14',
                lastname: 'Tes14',
                email: 'Tes14@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes15',
                firstname: 'Tes15',
                lastname: 'Tes15',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes16',
                firstname: 'Tes16',
                lastname: 'Tes16',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes17',
                firstname: 'Tes17',
                lastname: 'Tes17',
                email: 'Tes17@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes18',
                firstname: 'Tes18',
                lastname: 'Tes18',
                email: 'Tes18@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes19',
                firstname: 'Tes19',
                lastname: 'Tes19',
                email: 'Tes19@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes20',
                firstname: 'Tes20',
                lastname: 'Tes20',
                email: 'Tes20@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes21',
                firstname: 'Tes21',
                lastname: 'Tes21',
                email: 'Tes21@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes22',
                firstname: 'Tes22',
                lastname: 'Tes22',
                email: 'Tes22@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes23',
                firstname: 'Tes23',
                lastname: 'Tes23',
                email: 'Tes23@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes24',
                firstname: 'Tes24',
                lastname: 'Tes24',
                email: 'Tes24@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes25',
                firstname: 'Tes25',
                lastname: 'Tes25',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes26',
                firstname: 'Tes26',
                lastname: 'Tes26',
                email: 'Tes26@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes27',
                firstname: 'Tes27',
                lastname: 'Tes27',
                email: 'Tes27@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes28',
                firstname: 'Tes28',
                lastname: 'Tes28',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes29',
                firstname: 'Tes29',
                lastname: 'Tes29',
                email: 'Tes29@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes30',
                firstname: 'Tes30',
                lastname: 'Tes30',
                email: 'Tes30@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes31',
                firstname: 'Tes31',
                lastname: 'Tes31',
                email: 'Tes31@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes32',
                firstname: 'Tes32',
                lastname: 'Tes32',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes33',
                firstname: 'Tes33',
                lastname: 'Tes33',
                email: 'Tes33@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes34',
                firstname: 'Tes34',
                lastname: 'Tes34',
                email: 'Tes34@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes35',
                firstname: 'Tes35',
                lastname: 'Tes35',
                email: 'Tes35@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes36',
                firstname: 'Tes36',
                lastname: 'Tes36',
                email: 'Tes36@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes1',
                firstname: 'Tes1',
                lastname: 'Tes1',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes2',
                firstname: 'Tes2',
                lastname: 'Tes2',
                email: 'Tes2@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes3',
                firstname: 'Tes3',
                lastname: 'Tes3',
                email: 'Tes3@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes4',
                firstname: 'Tes4',
                lastname: 'Tes4',
                email: 'Tes4@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes5',
                firstname: 'Tes5',
                lastname: 'Tes5',
                email: 'Tes5@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes6',
                firstname: 'Tes6',
                lastname: 'Tes6',
                email: 'Tes6@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes7',
                firstname: 'Tes7',
                lastname: 'Tes7',
                email: 'Tes7@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes8',
                firstname: 'Tes8',
                lastname: 'Tes8',
                email: 'Tes8@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes9',
                firstname: 'Tes9',
                lastname: 'Tes9',
                email: 'Tes9@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes10',
                firstname: 'Tes10',
                lastname: 'Tes10',
                email: 'Tes10@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes11',
                firstname: 'Tes11',
                lastname: 'Tes11',
                email: 'Tes11@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes12',
                firstname: 'Tes12',
                lastname: 'Tes12',
                email: 'Tes12@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes13',
                firstname: 'Tes13',
                lastname: 'Tes13',
                email: 'Tes13@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes14',
                firstname: 'Tes14',
                lastname: 'Tes14',
                email: 'Tes14@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes15',
                firstname: 'Tes15',
                lastname: 'Tes15',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes16',
                firstname: 'Tes16',
                lastname: 'Tes16',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes17',
                firstname: 'Tes17',
                lastname: 'Tes17',
                email: 'Tes17@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes18',
                firstname: 'Tes18',
                lastname: 'Tes18',
                email: 'Tes18@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes19',
                firstname: 'Tes19',
                lastname: 'Tes19',
                email: 'Tes19@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes20',
                firstname: 'Tes20',
                lastname: 'Tes20',
                email: 'Tes20@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes21',
                firstname: 'Tes21',
                lastname: 'Tes21',
                email: 'Tes21@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes22',
                firstname: 'Tes22',
                lastname: 'Tes22',
                email: 'Tes22@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes23',
                firstname: 'Tes23',
                lastname: 'Tes23',
                email: 'Tes23@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes24',
                firstname: 'Tes24',
                lastname: 'Tes24',
                email: 'Tes24@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes25',
                firstname: 'Tes25',
                lastname: 'Tes25',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes26',
                firstname: 'Tes26',
                lastname: 'Tes26',
                email: 'Tes26@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes27',
                firstname: 'Tes27',
                lastname: 'Tes27',
                email: 'Tes27@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes28',
                firstname: 'Tes28',
                lastname: 'Tes28',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes29',
                firstname: 'Tes29',
                lastname: 'Tes29',
                email: 'Tes29@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes30',
                firstname: 'Tes30',
                lastname: 'Tes30',
                email: 'Tes30@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes31',
                firstname: 'Tes31',
                lastname: 'Tes31',
                email: 'Tes31@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes32',
                firstname: 'Tes32',
                lastname: 'Tes32',
                email: 'Tes1@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes33',
                firstname: 'Tes33',
                lastname: 'Tes33',
                email: 'Tes33@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes34',
                firstname: 'Tes34',
                lastname: 'Tes34',
                email: 'Tes34@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes35',
                firstname: 'Tes35',
                lastname: 'Tes35',
                email: 'Tes35@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            {
                id: 1000,
                username: 'Tes36',
                firstname: 'Tes36',
                lastname: 'Tes36',
                email: 'Tes36@gmail.com',
                birthdate: '1990-02-01',
                basicsalary: 8000000,
                status: 'PKWT',
                group: 'IT',
                description: '2023-10-10'
                
            },
            
        ];
    }

    constructor(private http: HttpClient) {}
    
    getEmployeesMini() {
        return Promise.resolve(this.getData().slice(0, 5));
    }

    getEmployeesSmall() {
        return Promise.resolve(this.getData().slice(0, 10));
    }

    getEmployeesMedium() {
        return Promise.resolve(this.getData().slice(0, 50));
    }

    getEmployeesLarge() {
        return Promise.resolve(this.getData().slice(0, 200));
    }

    getEmployeesXLarge() {
        return Promise.resolve(this.getData());
    }

};