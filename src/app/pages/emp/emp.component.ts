import { Component, OnInit, ViewChild, TemplateRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LayoutService } from '../../_metronic/layout';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { IconFieldModule } from 'primeng/iconfield';
import { InputIconModule } from 'primeng/inputicon';
import { TagModule } from 'primeng/tag';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CardModule } from 'primeng/card';
import { Employee } from './employee';
import { EmployeeData } from './employeedata';

@Component({
  selector: 'app-emp',
  templateUrl: './emp.component.html',
  standalone: true,
  imports: [CardModule,TableModule, TagModule, IconFieldModule, InputTextModule, InputIconModule, MultiSelectModule, DropdownModule, HttpClientModule, CommonModule],
  providers: [EmployeeData]
})
export class EmpComponent implements OnInit {
  employees!: Employee[];
  loading: boolean = true;
  activityValues: number[] = [0, 100];

  constructor(private layout: LayoutService,private employeeData: EmployeeData) {}

  ngOnInit(): void {
    this.employeeData.getEmployeesLarge().then((employees) => {
      this.employees = employees;
      this.loading = false;
      this.employees.forEach((employees) => (employees.description = new Date(<Date>employees.description)));
  });
  }
}
