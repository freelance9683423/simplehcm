export interface Employee {
    id?: number;
    username?: string;
    firstname?: string;
    lastname?: string;
    email?: string;
    birthdate?: string | Date;
    basicsalary?: number;
    status?: string;
    group?: string;
    description?: string | Date;
}