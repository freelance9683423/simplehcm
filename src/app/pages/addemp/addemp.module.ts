import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AddempComponent } from './addemp.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: AddempComponent,
      },
    ]),
  ],
})
export class AddempModule { }
