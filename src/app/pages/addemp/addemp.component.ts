import { Component } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {FormGroup,FormControl, Validators, FormsModule, ReactiveFormsModule, FormBuilder} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputNumberModule } from 'primeng/inputnumber';
import { ActivatedRoute, Router } from '@angular/router';

interface Status {
  name: string,
}

interface Group {
  groups: string,
}

interface Post {
  birth: Date;
}

@Component({
  selector: 'app-addemp',
  templateUrl: './addemp.component.html',
  styleUrl: './addemp.component.scss',
  standalone: true,
  imports: [InputNumberModule,DropdownModule,MultiSelectModule,MatCardModule,MatFormFieldModule, MatInputModule, FormsModule, ReactiveFormsModule],
})
export class AddempComponent 
{
  status!: Status[];
  group: Group[];
  selectedStatus!: Status[];
  selectedGroup!: Group[];
  reactiveForm: FormGroup;

  constructor(private builder: FormBuilder, private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.status = [
      { name: 'PKWT' },
      { name: 'PKWTT' },
      { name: 'RESIGN' },
    ];
    this.group = [
      { groups: 'IT' },
      { groups: 'HR' },
      { groups: 'MARKETING' },
      { groups: 'SALES' },
      { groups: 'FINANCE' },
      { groups: 'ACCOUNTANT' },
      { groups: 'TAX' },
      { groups: 'CONSULTANT' },
      { groups: 'OPERASIONAL' },
      { groups: 'TEASURY' },
    ];
    this.reactiveForm = this.builder.group({
      username: [null, Validators.required],
      firstname: [null, Validators.required],
      lastname: [null, Validators.required],
      emails: [null, [Validators.required, Validators.email]],
      birthdate: ['1990-01-01', Validators.required],
      basicsalary: [null, Validators.required],
      selectedStatus: new FormControl<Status[] | null>([{ name: 'PKWT'}]),
      selectedGroup: new FormControl<Group[] | null>([{ groups: 'IT'}]),
      joindate: ['1990-01-01', Validators.required],
    });
  }

  submit() {
    this.router.navigate(['/employee']);
  }

}
