import { Routes } from '@angular/router';

const Routing: Routes = [
  {
    path: 'dashboard',
    redirectTo: '/employee',
    pathMatch: 'full',
  },
  {
    path: 'employee',
    loadChildren: () =>
      import('./emp/emp.module').then((m) => m.EmpModule),
  },
  {
    path: 'editemployee',
    loadChildren: () =>
      import('./editemp/editemp.module').then((m) => m.EditempModule),
  },
  {
    path: 'addemployee',
    loadChildren: () =>
      import('./addemp/addemp.module').then((m) => m.AddempModule),
  },
  {
    path: '',
    redirectTo: '/employee',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'error/404',
  },
];

export { Routing };
